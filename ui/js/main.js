var Main = function(){
  this.audio = null;

  $('.player .play_button').click(() => {
    if (this.isPlaying()) {
      console.log('playing');
      this.stop();
    }

    $.ajax({
      url: '/song',
      dataType: 'json'
    }).done( data => {
      console.log(data);
      this.play(data.lyrics, data.filename)
    }).fail( data => {
      console.log("error");
      console.log(data);
    });
  });

  $('.player .stop_button').click(() => {
    if (this.isPlaying()) {
      console.log('playing');
      this.stop();
    }
  });
};


Main.prototype = {
  play: function(lyrics, filename) {
    this.playing = true;
    $('.lyrics').empty()
    for (lyric of lyrics) {
      console.log(lyric)
      $('.lyrics').append('<p>' + lyric + '</p>');
    }

    var audio = new Audio()
    this.audio = audio
    audio.src = filename
    audio.play()
  },

  stop: function() {
    if (!this.isPlaying) {
      return;
    }
    this.audio.pause();
    this.audio.currentTime = 0;
    this.audio.src = '';
    this.audio = null;
  },

  isPlaying: function() {
    return this.audio != null && !this.audio.paused
  }
}


$(document).ready(function(){
  window.main = new Main()
})

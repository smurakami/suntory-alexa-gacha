import subprocess
import glob
import random
import time
import pydub
from pydub import AudioSegment

from generator import Generator


def main():
    generator = Generator()

    while True:
        input('hit enter key:')
        lyrics, filename =  generator.generate()

        print('\n'.join(lyrics))
        subprocess.Popen('afplay {}'.format(filename), shell=True).communicate()



main()

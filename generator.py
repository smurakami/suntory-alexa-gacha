import subprocess
import glob
import random
import time
import pydub
from pydub import AudioSegment
import re

bars = [
    0.00,
    4.05,
    7.23,
    11.13,
    15.03,
    18.23,
    22.12,
    38.14,
]


songs = [
    ["おさらが　ひとつ　あったとさ", "おせんべい　ひとつ　でてきて", "コップが　ひとつ　でてくると", "あめだま　ふたつ　おちてきた", "はしごが　ひとつ　ありまして", "おひさま　ひとつ　でてきたよ", "ごまが　ひとつ　でてくると", ],
    ["おべんとうばこ　ひとつ　あったとさ", "とうふが　ひとつ　でてきて　", "ナイフが　ひとつ　でてくると", "にんじん　ふたつ　おちてきた", "かいだん　ひとつ　ありまして", "くもが　ひとつ　でてきたよ", "ごまが　ふたつ　でてくると", ],
    ["スプーン　ひとつ　あったとさ　", "おだんご　ふたつ　でてきて", "フォークが ふたつ でてくると", "おまめが　みっつ　おちてきた", "テントが　ひとつ　ありまして", "みかづき　ひとつ　でてきたよ", "つららが　ひとつ　でてくると", ],
    ["おやまが　ひとつ　あったとさ", "たらこが　ふたつ　でてきて", "タマゴが　ひとつ　でてくると", "じゃがいも　よっつ　おちてきた", "はっぱが　ふたつ　はえまして", "めだまやき　ひとつ　やけたよ", "つららが ふたつ　でてくると", ],
    ["いけが　ひとつ　あったとさ", "おもちが　みっつ　でてきて", "おにぎり　ふたつ　でてくると", "とびらが　ひとつ　ありました", "たけのこ　ひとつ　はえまして", "パンが　ひとつ　やけたよ", "いとが　いっぽん　のびますと", ],
    ["まるを　ひとつ　かきまして", "ぼうが　いっぽん　はえてきて", "さかなが　ふたつ　でてくると", "まどが　ひとつ　ありました", "つるが　いっぽん　はえまして", "ぎょうざが　ふたつ　やけたよ", "いとが　にほん　のびますと", ],
    ["さんかく　ひとつ　かきまして", "ぼうが　にほん　はえてきて", "あめが　ザーザー　ふりますと", "やねが　ひとつ　ありました", "くさが　ぼうぼう　はえまして", "ばってん　ひとつ　でてきたよ", "まるを　ひとつ　かきますと", ],
    ["しかくを　ひとつ　かきまして", "ぼうが　さんぼん　はえてきて", "あられが ポツポツ ふりますと", "えんとつ　ひとつ　ありました", "いとが　いっぽん　のびまして", "ばってん　ふたつ　でてきたよ", "さんかく　ひとつ　かきますと", ],
    ["せんを　いっぽん　かきまして", "ぼうが　よんほん　はえてきて", "かみなり ごろごろ おちますと", "おはなが　ひとつ　はえてきた", "いとが　にほん　のびまして", "うずまき　ひとつ　でてきたよ", "しかくを　ひとつ　かきますと", ],
    ["まるかいてちょん、まるかいてちょん", "ぼうが　たくさん　はえてきて", "ほしが　キラキラ　ふりますと", "えだが　さんぼん　はえてきた", "いとが　さんぼん　のびまして", "うずまき　ふたつ　でてきたよ", "せんを　いっぽん　かきますと", ],
]


class Generator:
    def __init__(self):
        start_time = time.time()
        sounds = glob.glob('sounds/*.mp3')
        sounds = list(sorted(sounds))
        sounds = [AudioSegment.from_mp3(sound) for sound in sounds]
        print('loading duration: ', time.time() - start_time)
        self.sounds = sounds

    def generate(self):
        sounds = self.sounds

        start_time = time.time()
        fade_duration = 0.5
        self.clean()
        # subprocess.Popen('rm tmp/*', shell=True).communicate()
        lyrics = []
        segments = []

        for index, (start, end) in enumerate(zip(bars, bars[1:])):
            i = random.randint(0, len(sounds) - 1)

            lyric = songs[i][index]
            lyrics.append(lyric)

            segment = sounds[i]

            if index > 0:
                start -= fade_duration

            if index < len(bars) - 1:
                end += fade_duration

            segment = segment[int(start * 1000):int(end * 1000)]
            segments.append(segment)

            # p = subprocess.Popen('ffmpeg -i {} -ss {} -t {} -codec copy tmp/{:04d}.mp3'.format(
            #     file, start, end - start, index
            #     ), shell=True)

            # ps.append(p)

        # for p in ps:
        #     p.communicate()

        output = segments[0]
        for segment in segments[1:]:
            output = output.append(segment, crossfade=int(fade_duration * 1000 * 2))


        filename = 'tmp/{}.wav'.format(time.time())

        output.export(open(filename, 'wb'), format='wav')

        print('duration: ', time.time() - start_time)


        # subprocess.Popen('ffmpeg -i "concat:{}" -codec copy tmp/output.mp3'.format(
        #     '|'.join(sorted(glob.glob('tmp/*.mp3')))
        #     ), shell=True).communicate()

        return lyrics, filename

    def clean(self):
        files = glob.glob('tmp/*.wav')
        now = time.time()
        for file in files:
            gen_time = float(re.search(r'\d+.\d+', file).group(0))
            if now - gen_time > 60 * 5:
                subprocess.Popen('rm {}'.format(file), shell=True).communicate()

